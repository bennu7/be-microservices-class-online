const { Media } = require("../models");
const isBase64 = require("is-base64");
const base64Img = require("base64-img");
const fs = require("fs");

module.exports = {
	getAllMedia: async (req, res) => {
		try {
			const dataMedia = await Media.findAll({
				attributes: ["id", "image"],
			});

			if (dataMedia.length === 0) {
				return res.status(404).json({
					message: "Data not found",
				});
			}

			const mappedMedia = dataMedia.map((media) => {
				media.image = `${req.get("host")}/images/${media.image
					.split("/")
					.pop()}`;

				return media;
			});

			return res.status(200).json({
				message: "Success",
				data: mappedMedia,
			});
		} catch (error) {
			console.log(error);
			res.status(500).json({
				message: "Internal server error",
			});
		}
	},

	getMediaById: async (req, res) => {
		try {
			const { id } = req.params;

			const dataMedia = await Media.findOne({
				where: {
					id,
				},
				attributes: ["id", "image"],
			});

			if (!dataMedia) {
				return res.status(404).json({
					message: "Data not found",
				});
			}

			dataMedia.image = `${req.get("host")}/images/${dataMedia.image
				.split("/")
				.pop()}`;

			return res.status(200).json({
				message: "Success",
				data: dataMedia,
			});
		} catch (error) {
			console.log(error);
			return res.status(500).json({
				message: "Internal server error",
			});
		}
	},

	postMedia: async (req, res) => {
		try {
			const image = req.body.image;

			// image harus bertipe base 64
			if (!isBase64(image, { mimeRequired: true })) {
				return res.status(400).json({
					status: false,
					message: "Image must be base64",
				});
			}

			base64Img.img(
				image,
				"public/images",
				Date.now(),
				async (err, filepath) => {
					if (err) {
						console.log(err);
						return res.status(400).json({
							status: false,
							message: "Failed upload image",
						});
					}

					const filename = filepath.split("/").pop();

					// simpan data image ke database
					const dataMedia = await Media.create({
						image: `images/${filename}`,
					});

					return res.status(201).json({
						message: "Success upload image",
						data: {
							id: dataMedia.id,
							image: `${req.get("host")}/images/${filename}')}`,
						},
					});
				}
			);
		} catch (error) {
			console.log(error);
			res.status(500).json({
				status: false,
				data: "Internal server error",
			});
		}
	},

	deleteMedia: async (req, res) => {
		try {
			const { id } = req.params;
			console.log(id);

			const dataMedia = await Media.findOne({
				where: {
					id,
				},
			});

			if (!dataMedia) {
				return res.status(404).json({
					message: "Data not found",
				});
			}

			// hapus file image
			fs.unlink(`public/${dataMedia.image}`, (err) => {
				if (err) {
					return res.status(400).json({
						status: false,
						message: "Failed delete image",
					});
				}
				console.log("File deleted!");
			});

			await Media.destroy({
				where: {
					id,
				},
			});

			return res.status(200).json({
				status: true,
				message: "Success delete media",
			});
		} catch (error) {
			console.log(error);
			res.status(500).json({
				message: "Internal server error",
			});
		}
	},
};
