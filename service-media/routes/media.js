const express = require("express");
const router = express.Router();

const mediaController = require("../controllers/mediaController");

/* GET users listing. */
router.get("/", mediaController.getAllMedia);
router.get("/:id", mediaController.getMediaById);
router.post("/", mediaController.postMedia);
router.delete("/delete/:id", mediaController.deleteMedia);

module.exports = router;
