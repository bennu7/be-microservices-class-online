const express = require("express");
const router = express.Router();

const mediaRoute = require("./media");

router.use("/media", mediaRoute);

module.exports = router;
