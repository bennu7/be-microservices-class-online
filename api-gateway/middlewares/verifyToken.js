require("dotenv").config();
const jwt = require("jsonwebtoken");
const { request, response } = require("express");
const { JWT_SECRET_KEY } = process.env;
const { catchError, responseHelper } = require("../helpers/response");

module.exports = {
	verifyToken: async (req = request, res = response, next) => {
		try {
			let getTokenBearear = req.headers.authorization;
			const token = getTokenBearear.split(" ");
			token.shift();
			const finalToken = token.join(" ");

			const verifyToken = await jwt.verify(finalToken, JWT_SECRET_KEY);
			if (!verifyToken) {
				return responseHelper(res, 401, "failed", "Unauthorized");
			}

			req.user = verifyToken.payload;
			next();
		} catch (error) {
			catchError(error, res);
		}
	},
};
