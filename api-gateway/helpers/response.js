// *UTILS ERROR HANDLER*
const { response, request } = require("express");
const responseHelper = (
	res = response,
	statusCode = 0,
	status,
	message = "",
	{ ...data } = {}
) => {
	return res.status(statusCode).send({
		status,
		message,
		...data,
	});
};

const catchError = (err, res = response) => {
	console.log("err api-gateway => ", err);

	switch (err.name) {
		case "AxiosError":
			return res.status(400).json({
				status: "ERR_BAD_REQUEST axios " + err.message,
				message: err.response.data.message || err.message,
			});
	}

	switch (err.message) {
		case "invalid token":
			return res.status(401).send({
				status: err.name || "invalid jwt token",
				message: err.message,
			});
		default:
			return res.status(500).send({
				status: "failed",
				message: err.message,
			});
	}
};

module.exports = { responseHelper, catchError };
