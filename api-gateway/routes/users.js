require("dotenv").config();
const express = require("express");
const router = express.Router();
const {
	getUser,
	registerUser,
	updateUser,
} = require("../handler/users/userHandler");
const { verifyToken } = require("../middlewares/verifyToken");
const { loginUser, logoutUser } = require("../handler/users/authHandler");

router.get("/", verifyToken, getUser);
router.post("/register", registerUser);
router.put("/update", verifyToken, updateUser);
router.post("/login", loginUser);
router.post("/logout", verifyToken, logoutUser);

module.exports = router;
