require("dotenv").config();
const express = require("express");
const router = express.Router();
const { APP_NAME } = process.env;
const { createMedia, getAllMediaData, getById } = require("../handler/media");

router.post("/", createMedia);
router.get("/", getAllMediaData);
router.get("/:id", getById);

module.exports = router;
