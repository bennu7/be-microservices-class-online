const express = require("express");
const router = express.Router();
const courseRoute = require("./courses");
const userRoute = require("./users");
const mediaRoute = require("./media");
const orderRoute = require("./order");
const paymentRoute = require("./payment");
const refreshTokenRoute = require("./refreshToken");

router.use("/users", userRoute);
router.use("/course", courseRoute);
router.use("/media", mediaRoute);
router.use("/order", orderRoute);
router.use("/payment", paymentRoute);
router.use("/refresh_token", refreshTokenRoute);

module.exports = router;
