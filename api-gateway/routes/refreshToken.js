require("dotenv").config();
const express = require("express");
const router = express.Router();
const { refresh_token } = require("../handler/refresh-tokens/refresh_token");

router.post("/", refresh_token);

module.exports = router;
