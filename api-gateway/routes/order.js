require('dotenv').config()
const express = require('express');
const router = express.Router();
const {APP_NAME} = process.env


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send(`respond with a resource from ENV ${APP_NAME}`);
});

module.exports = router;
