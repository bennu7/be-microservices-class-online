require("dotenv").config();
const jwt = require("jsonwebtoken");
const { request, response } = require("express");
const {
	URL_SERVICE_USERS,
	JWT_SECRET_KEY,
	JWT_ACCESS_TOKEN_EXPIRED,
	JWT_SECRET_REFRESH_TOKEN,
	JWT_REFRESH_TOKEN_EXPIRED,
} = process.env;
const axios = require("axios");
const { responseHelper, catchError } = require("../../helpers/response");

module.exports = {
	loginUser: async (req = request, res = response) => {
		try {
			const user = await axios.post(
				`${URL_SERVICE_USERS}/api/users/login`,
				req.body
			);
			const message = user.data.message;
			const data = user.data.data;
			const statusCode = user.status;
			const statusMessage = user.data.status;

			const payload = {
				id: user.data.data.id,
				name: user.data.data.name,
				role: user.data.data.role,
				email: user.data.data.email,
				profession: user.data.data.profession,
			};

			const token = jwt.sign({ payload }, JWT_SECRET_KEY, {
				expiresIn: JWT_ACCESS_TOKEN_EXPIRED,
			});
			const refreshToken = jwt.sign({ payload }, JWT_SECRET_REFRESH_TOKEN, {
				expiresIn: JWT_REFRESH_TOKEN_EXPIRED,
			});

			await axios.post(`${URL_SERVICE_USERS}/api/refresh_token`, {
				token: refreshToken,
				user_id: data.id,
			});

			return responseHelper(res, statusCode, statusMessage, message, {
				data: {
					token,
					refreshToken,
				},
			});
		} catch (error) {
			catchError(error, res);
		}
	},
	logoutUser: async (req = request, res = response) => {
		try {
			const id = req.user.data.id;

			const user = await axios.post(`${URL_SERVICE_USERS}/api/users/logout`, {
				user_id: id,
			});

			responseHelper(res, user.data.statusCode, user.data.message);
		} catch (error) {
			catchError(error, res);
		}
	},
};
