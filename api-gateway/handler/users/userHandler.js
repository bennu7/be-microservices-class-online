const { URL_SERVICE_USERS } = process.env;
const axios = require("axios");
const { response, request } = require("express");
const { responseHelper, catchError } = require("../../helpers/response");

module.exports = {
	getUser: async (req = request, res = response) => {
		try {
			const { id } = req.user;
			const data = await axios.get(`${URL_SERVICE_USERS}/api/users/${id}`);
			return res.json(data.data);
		} catch (error) {
			catchError(error, res);
		}
	},
	registerUser: async (req = request, res = response) => {
		try {
			const data = await axios.post(
				`${URL_SERVICE_USERS}/api/users/register`,
				req.body
			);
			return res.json(data.data);
		} catch (error) {
			catchError(error, res);
		}
	},
	updateUser: async (req = request, res = response) => {
		try {
			const { id } = req.user;
			const media = await axios.put(
				`${URL_SERVICE_USERS}/api/users/${id}`,
				req.body
			);
			return res.json(media.data);
		} catch (error) {
			catchError(error, res);
		}
	},
};
