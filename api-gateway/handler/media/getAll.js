const apiAdapter = require("../../adapter/apiAdapter");

const { URL_SERVICE_MEDIA } = process.env;
const axios = require("axios");

const api = apiAdapter(URL_SERVICE_MEDIA);

module.exports = async (req, res) => {
	try {
		// const media = await api.get("/media");
		// return res.json(media.data);

		const media = await axios.get(`${URL_SERVICE_MEDIA}/media`);
		return await res.json(media.data);
	} catch (error) {
		console.log("error =>", error);
		if (error.code == "ECONNREFUSED") {
			return res
				.status(500)
				.json({ status: "error", message: "Service unavailable" });
		}
		// return res.status(400).json(error);
		const { status, data } = error.response;
		return res.status(status).json(data);
	}
};
