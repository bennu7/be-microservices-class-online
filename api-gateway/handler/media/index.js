const createMedia = require("./create");
const getAllMediaData = require("./getAll");
const getById = require("./getById");

module.exports = { createMedia, getAllMediaData, getById };
