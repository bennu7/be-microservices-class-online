const apiAdapter = require("../../adapter/apiAdapter");
const axios = require("axios");

const { URL_SERVICE_MEDIA } = process.env;

const api = apiAdapter(URL_SERVICE_MEDIA);

module.exports = async (req, res) => {
	try {
		const media = await axios.post(`${URL_SERVICE_MEDIA}/media`, req.body);

		console.log(media.data);
		return await res.json(media.data);
	} catch (error) {
		console.log("error =>", error);
		if (error.code === "ECONNREFUSED") {
			return res
				.status(500)
				.json({ status: "error", message: "service unavailable" });
		}

		const { status, data } = error.response;
		return res.status(status).json(data);
		// return res.status(400).json(error);
	}
};
