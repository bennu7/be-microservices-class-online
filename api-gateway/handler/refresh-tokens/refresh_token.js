const jwt = require("jsonwebtoken");
const {
	URL_SERVICE_USERS,
	JWT_SECRET_KEY,
	JWT_SECRET_REFRESH_TOKEN,
	JWT_ACCESS_TOKEN_EXPIRED,
	JWT_REFRESH_TOKEN_EXPIRED,
} = process.env;
const { catchError, responseHelper } = require("../../helpers/response");
const { response, request } = require("express");
const axios = require("axios");

module.exports = {
	refresh_token: async (req = request, res = response) => {
		try {
			const { refresh_token, email } = req.body;
			if (!refresh_token) {
				return responseHelper(res, 400, "failed", "invalid token");
			}

			const posted = await axios.post(
				`${URL_SERVICE_USERS}/api/refresh_token/new`,
				{},
				{
					params: { refresh_token },
				}
			);
			const messageResponse = posted.data.message;
			const statusResponse = posted.data.status;
			const statusCodeResponse = posted.status;

			const verifyToken = await jwt.verify(
				refresh_token,
				JWT_SECRET_REFRESH_TOKEN
			);

			if (verifyToken.email !== email || !verifyToken) {
				return responseHelper(res, 400, "failed", "invalid token");
			}

			// assign new token with payload
			const payload = {
				id: verifyToken.id,
				name: verifyToken.name,
				email: verifyToken.email,
				role: verifyToken.role,
				profession: verifyToken.profession,
			};
			const token = await jwt.sign(payload, JWT_SECRET_KEY, {
				expiresIn: JWT_ACCESS_TOKEN_EXPIRED,
			});

			return responseHelper(
				res,
				statusCodeResponse,
				statusResponse,
				messageResponse,
				{
					token,
				}
			);
		} catch (error) {
			catchError(error, res);
		}
	},
};
