const { User } = require("../models");
const bcrypt = require("bcrypt");
const asyncHandler = require("express-async-handler");
const Validator = require("fastest-validator");
const v = new Validator();
const { catchError } = require("../helpers/errorHandler");
const { responseHelper } = require("../helpers/response");
const { request, response } = require("express");

module.exports = {
	/*
	 * @desc    Get all users
	 * @route   GET /users
	 */
	getAllUser: async (req = request, res = response) => {
		try {
			const data = await User.findAll({
				attributes: {
					exclude: ["password", "createdAt", "updatedAt"],
				},
			});

			responseHelper(res, 200, true, "List of Users", { data });
		} catch (err) {
			catchError(err, res);
		}
	},

	/*
	 * @desc    Get user by id
	 * @route   GET /users/:id
	 */
	getUserById: async (req = request, res = response) => {
		try {
			const { id } = req.params;

			const findUser = await User.findByPk(id, {
				attributes: {
					exclude: ["password", "createdAt", "updatedAt", "deletedAt"],
				},
			});

			if (!findUser) {
				responseHelper(res, 404, false, "User not found");
			}

			responseHelper(res, 200, true, "User found", { data: findUser });
		} catch (err) {
			catchError(err, res);
		}
	},

	/*
	 * @desc    Create user
	 * @route   POST /users
	 */
	registerUser: async (req = request, res = response) => {
		try {
			const schema = {
				name: "string|empty:false",
				email: "email|empty:false",
				password: "string|min:6",
				profession: "string|optional",
			};
			const validate = v.validate(req.body, schema);

			if (validate.length) {
				return responseHelper(res, 400, false, validate);
			}

			const { name, email, password, profession } = req.body;

			const findUser = await User.findOne({
				where: {
					email,
				},
			});

			if (findUser) {
				return responseHelper(res, 409, false, "Email already exists");
			}

			const hashedPassword = await bcrypt.hash(password, 10);

			const data = await User.create({
				name,
				email,
				password: hashedPassword,
				profession,
			});

			return responseHelper(res, 201, true, "User created", { data });
		} catch (err) {
			catchError(err, res);
		}
	},
	/*
	 * @desc    Create user
	 * @route   POST /users
	 */
	updateUser: async (req = request, res = response) => {
		try {
			const schema = {
				name: "string|empty:false",
				email: "email|empty:false",
				password: "string|min:6",
				profession: "string|optional",
				avatar: "string|optional",
			};

			const validate = v.validate(req.body, schema);
			if (validate.length) {
				responseHelper(res, 409, false, validate);
			}

			const { id } = req.params;
			const { name, email, password, profession, avatar } = req.body;

			const findUser = await User.findByPk(id);
			if (!findUser) {
				responseHelper(res, 404, false, "User not found");
			}

			if (email) {
				const checkEmail = await User.findOne({
					where: {
						email,
					},
				});

				if (checkEmail && email !== findUser.email) {
					responseHelper(res, 409, false, "Email already exists");
				}
			}

			const updatedPassword = await bcrypt.hash(password, 10);

			const updated = await User.update({
				email,
				password: updatedPassword,
				name,
				profession,
				avatar,
			});
			console.log(updated);

			responseHelper(res, 200, true, "User updated", {
				id: findUser.id,
				email,
				name,
				profession,
				avatar,
			});
		} catch (err) {
			catchError(err, res);
		}
	},

	/*
	 * @desc    Delete user
	 * @route   DELETE /users/:id
	 */
	deleteUser: async (req = request, res = response) => {
		try {
			const { id } = req.params;

			const findUser = await User.findByPk(id);
			if (!findUser) {
				responseHelper(res, 404, false, "User not found");
			}

			await User.destroy({
				where: {
					id,
				},
			});

			responseHelper(res, 200, true, "User deleted");
		} catch (err) {
			catchError(err, res);
		}
	},
};
