require("dotenv").config();
const { User, RefreshToken } = require("../models");
const { request, response } = require("express");
const Validator = require("fastest-validator");
const v = new Validator();
const { responseHelper } = require("../helpers/response");
const { catchError } = require("../helpers/errorHandler");
const jwt = require("jsonwebtoken");

module.exports = {
	/*
	 * @desc Create new refresh token
	 * @route POST /refresh_token
	 */
	createRefreshToken: async (req = request, res = response) => {
		try {
			const schema = {
				token: "string|empty:false",
				user_id: "number|empty:false",
			};
			const validate = v.validate(req.body, schema);
			if (validate.length) {
				return responseHelper(res, 400, "error validate", validate);
			}

			const { user_id, token } = req.body;
			const findUser = await User.findByPk(user_id);
			if (!findUser) {
				return responseHelper(res, 404, "error", "user not found");
			}

			const createRefreshToken = await RefreshToken.create({
				user_id,
				token,
			});
			return responseHelper(res, 200, "success", "refresh token created", {
				id: createRefreshToken.id,
			});
		} catch (err) {
			catchError(err, res);
		}
	},

	/*
	 * @desc get token
	 * @route POST /refresh_token/new
	 */
	postNewRefreshToken: async (req = request, res = response) => {
		try {
			const { refresh_token } = req.query;
			const findRefreshToken = await RefreshToken.findOne({
				where: {
					token: refresh_token,
				},
			});

			if (!findRefreshToken) {
				return responseHelper(res, 400, "error", "invalid token");
			}

			return responseHelper(res, 200, "success", "success refresh new token", {
				data: findRefreshToken,
			});
		} catch (err) {
			catchError(err, res);
		}
	},
};
