const { request, response } = require("express");
const { User, RefreshToken } = require("../models");
const Validator = require("fastest-validator");
const v = new Validator();
const { responseHelper } = require("../helpers/response");
const { catchError } = require("../helpers/errorHandler");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = {
	/*
	 * @desc    Auth login user (email & password)
	 * @route   POST /users/login
	 */
	loginUser: async (req = request, res = response) => {
		try {
			const schema = {
				email: "email|empty:false",
				password: "string|min:6",
			};
			const validate = v.validate(req.body, schema);
			if (validate.length) {
				responseHelper(res, 400, false, "Validation error", validate);
			}

			const { email, password } = req.body;
			const findUser = await User.findOne({
				where: { email: email },
				exclude: ["createdAt", "updatedAt", "deletedAt", "password"],
			});

			if (!findUser) {
				responseHelper(res, 404, false, "invalid email or password");
			}

			const isValidPassword = await bcrypt.compare(password, findUser.password);
			if (!isValidPassword) {
				responseHelper(res, 400, false, "invalid email or password");
			}

			// const generateToken = jwt.sign(
			// 	{
			// 		id: findUser.id,
			// 		email: findUser.email,
			// 		role: findUser.role,
			// 	},
			// 	process.env.SECRET_KEY
			// );
			//
			// //* set cookie token
			// res.cookie("token", generateToken, {
			// 	httpOnly: true,
			// 	expires: new Date(Date.now() + 3 * 60 * 60 * 1000),
			// });

			// const token = await findUser.generateToken();
			responseHelper(res, 200, true, "User logged in", { data: findUser });
		} catch (err) {
			catchError(err, res);
		}
	},

	/*
	 * @desc    Auth logout user
	 * @route   POST /users/logout
	 */
	logoutUser: async (req = request, res = response) => {
		try {
			const { user_id } = req.body;
			const findUser = await User.findByPk(user_id);
			if (!findUser) {
				return responseHelper(res, 404, false, "User not found", findUser);
			}

			await RefreshToken.destroy({
				where: { user_id },
			});

			responseHelper(
				res,
				200,
				true,
				`User ${findUser.dataValues.name} logged out & refresh token deleted`
			);
		} catch (err) {
			catchError(err, res);
		}
	},
};
