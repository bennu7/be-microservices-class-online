"use strict";
require("dotenv").config();
const faker = require("faker");

const bcrypt = require("bcrypt");
const { Sequelize, DataTypes, QueryInterface } = require("sequelize");
const { DB_NAME, DB_USERNAME, DB_PASSWORD, DB_HOSTNAME, DB_DIALECT } =
	process.env;
const sequelize = new Sequelize(DB_NAME, DB_USERNAME, DB_PASSWORD, {
	host: DB_HOSTNAME,
	dialect: DB_DIALECT,
});
const queryInterface = sequelize.getQueryInterface();

module.exports = {
	async up() {
		// *dummy data with faker
		// var dummyJSON = [];
		// for (let index = 0; index < 10; index++) {
		// 	const name = await faker.name.firstName();
		// 	dummyJSON.push({
		// 		name: name,
		// 		profession: await faker.finance.accountName(),
		// 		role: "student",
		// 		email: await faker.internet.email(),
		// 		avatar: await faker.image.avatar(),
		// 		password: await byrcpt.hash(name, 10),
		// 		createdAt: new Date(),
		// 		updatedAt: new Date(),
		// 	});
		// }
		// await queryInterface.bulkInsert("users", dummyJSON, {});

		await queryInterface.bulkInsert(
			"users",
			[
				{
					name: "benlur",
					profession: "Admin Micro",
					role: "admin",
					email: "benlursecond@gmail.com",
					avatar: "https://avatars.githubusercontent.com/u/76883098?v=4",
					password: await bcrypt.hash("iniadmin", 10),
					createdAt: new Date(),
					updatedAt: new Date(),
				},
				{
					name: "Bennu",
					profession: "Back End Developer",
					role: "student",
					email: "bennu@gmail.com",
					avatar: "https://avatars.githubusercontent.com/u/76883098?v=4",
					password: await bcrypt.hashSync("inirahasia", 10),
					createdAt: new Date(),
					updatedAt: new Date(),
				},
			],
			{}
		);
	},

	async down() {
		await queryInterface.bulkDelete("users", null, {});
	},
};
