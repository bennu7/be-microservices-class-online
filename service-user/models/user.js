'use strict';
const {
    Model
} = require('sequelize');

const {DataTypes, Sequelize} = require("sequelize")


// module.exports = (sequelize, DataTypes) => {
module.exports = (sequelize) => {
    class User extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */

        static associate(models) {
            this.hasOne(models.RefreshToken, {foreignKey: "user_id", as: "refresh_token"})
        }
    }

    User.init({
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        avatar: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        role: {
            type: DataTypes.ENUM,
            values: ["admin", "student"],
            allowNull: false,
            defaultValue: "student"
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
        profession: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: true
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: 'User',
        tableName: 'users',
        timestamps: true,
    });
    return User;
};