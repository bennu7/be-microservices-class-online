"use strict";
const { Model } = require("sequelize");
const { DataTypes } = require("sequelize");

// const {DataTypes, Sequelize} = require("../config/database")
module.exports = (sequelize) => {
	class RefreshToken extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
		}
	}

	RefreshToken.init(
		{
			token: {
				type: DataTypes.TEXT,
				allowNull: false,
			},
			user_id: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			createdAt: {
				type: DataTypes.DATE,
				allowNull: false,
			},
			updatedAt: {
				type: DataTypes.DATE,
				allowNull: true,
			},
			deletedAt: {
				type: DataTypes.DATE,
				allowNull: true,
			},
		},
		{
			sequelize,
			modelName: "RefreshToken",
			tableName: "refresh_tokens",
			timestamps: true,
		}
	);
	return RefreshToken;
};
