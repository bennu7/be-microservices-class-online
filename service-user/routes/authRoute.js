const express = require("express");
const router = express.Router();
const { loginUser, logoutUser } = require("../controllers/authController");
const { validate } = require("../helpers/validator");

// auth routes
router.post("/login", loginUser);
router.post("/logout", logoutUser);

module.exports = router;
