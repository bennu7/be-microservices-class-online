const express = require("express");
const router = express.Router();
const userRoute = require("./userRoute");
const tokenRoute = require("./tokenRoute");
const authRoute = require("./authRoute");

router.use("/users", userRoute);
// router.use("/auth", authRoute);
router.use("/refresh_token", tokenRoute);

module.exports = router;
