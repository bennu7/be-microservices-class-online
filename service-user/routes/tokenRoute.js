const express = require("express");
const router = express.Router();
const {
	createRefreshToken,
	postNewRefreshToken,
} = require("../controllers/tokenController");

router.post("/", createRefreshToken);
router.post("/new", postNewRefreshToken);

module.exports = router;
