const express = require("express");
const router = express.Router();
const {
	getAllUser,
	getUserById,
	registerUser,
	deleteUser,
	updateUser,
} = require("../controllers/userController");
const { loginUser, logoutUser } = require("../controllers/authController");
const { validate } = require("../helpers/validator");

// user routes
router.get("/", getAllUser);
router.get("/:id", getUserById);
router.post("/register", registerUser);
router.delete("/:id", deleteUser);
router.put("/:id", updateUser);

// auth routes
router.post("/login", loginUser);
router.post("/logout", logoutUser);

module.exports = router;
