"use strict";
const {queryInterface, Sequelize} = require("../config/database")

module.exports = {
    async up() {
        await queryInterface.createTable("refresh_tokens", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            token: {
                type: Sequelize.TEXT,
                allowNull: false,
            },
            user_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            createdAt: {
                allowNull: true,
                type: Sequelize.DATE,
            },
            deletedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });

        await queryInterface.addConstraint("refresh_tokens", {
            type: "foreign key",
            name: "REFRESH_TOKENS_USER_ID",
            fields: ["user_id"],
            references: {
                table: "users",
                field: "id",
            },
        });
    },

    async down() {
        await queryInterface.dropTable("refresh_tokens");
    },
};
