require("dotenv").config();
const {DB_NAME, DB_USERNAME, DB_PASSWORD, DB_HOSTNAME, DB_DIALECT} =
    process.env;

module.exports = {
    development: {
        username: DB_USERNAME,
        password: DB_PASSWORD,
        database: DB_NAME,
        host: DB_HOSTNAME,
        dialect: DB_DIALECT,
        paranoid: true,
        // *connection pooling
        pool:{
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000,
        }
    },
};
