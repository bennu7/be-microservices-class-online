require("dotenv").config();
const {DB_NAME, DB_USERNAME, DB_PASSWORD, DB_HOSTNAME, DB_DIALECT} =
    process.env;

const { Sequelize, DataTypes, QueryInterface } = require("sequelize");
const sequelize = new Sequelize(DB_NAME, DB_USERNAME, DB_PASSWORD, {
    host: DB_HOSTNAME,
    dialect: DB_DIALECT,
});
const queryInterface = sequelize.getQueryInterface();

module.exports = {queryInterface, Sequelize, DataTypes}