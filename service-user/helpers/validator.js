const {body, validationResult, param} = require("express-validator")

exports.validate = (method = "") => {
    switch (method) {
        case "createUser": {
            return [
                body("name", "Name is required").notEmpty().isString(),
                body("role", "Role is required").isIn(["admin", "student"]).isString(),
                body("email", "Email is required").notEmpty().isEmail().normalizeEmail(),
                body("password", "Password is required").notEmpty().isString().isLength({min: 6}),
                body("profession", "Profession is required").optional().isString(),
            ]
        }

        case "getById": {
            return  [
                param("id", "Id is required").notEmpty().isInt(),
            ]
        }
    }
}

