const {response} = require("express");
const responseHelper = (res = response, statusCode = 0, status, message = "", {...data} = {}) => {
    return res.status(statusCode).send({
        status,
        message,
        ...data,
    });
}

module.exports = {responseHelper};