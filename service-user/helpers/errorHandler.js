// *UTILS ERROR HANDLER*
const { response } = require("express");
const { Error } = require("sequelize");

class ErrorResponse extends Error {
	constructor(message = "", statusCode = 0) {
		super(message);
		this.code = statusCode;

		Error.captureStackTrace(this, this.constructor);
	}
}

const errorHandler = (err, req, res, next) => {
	let errors = { ...err };
	console.log("errors => ", errors);
	console.log("errors.code => ", errors.code);
	console.log("err.code => ", err.code);

	errors.message = err.message;

	switch (errors.code) {
		case 400: {
			const message = errors.message || "Bad Request";
			errors = new ErrorResponse(message, 400);
			break;
		}
	}
};

const catchError = (err, res = response) => {
	console.log("err.name => ", err.name);
	console.log("err service-user => ", err);
	switch (err.name) {
		case "SequelizeValidationError":
			return res.status(400).send({
				status: "failed SequelizeValidationError",
				message: err.message,
			});
		case "SequelizeUniqueConstraintError":
			return res.status(409).send({
				status: "failed SequelizeUniqueConstraintError",
				message: err.message,
			});
		default:
			return res.status(500).send({
				status: "failed",
				message: err.message,
			});
	}
};

module.exports = { errorHandler, catchError };
